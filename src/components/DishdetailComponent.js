import React, { Component } from 'react';
import { Card , CardBody ,CardImg, CardImgOverlay,CardText, CardTitle , 
    Breadcrumb, BreadcrumbItem, Modal, ModalHeader, ModalBody,
     Row, Col,Form, FormGroup, Input, Label,Button} from 'reactstrap';
import {Link} from 'react-router-dom';
import { Media } from 'reactstrap';
//import {Link} from 'react-router-dom';    
import { Control, LocalForm, Errors, Field } from 'react-redux-form';
import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';
import { FadeTransform, Fade, Stagger } from 'react-animation-components';



const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => val && (val.length >= len);


class CommentForm extends Component  {

    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false
         };
    
     this.toggleModal = this.toggleModal.bind(this);
     this.handleSubmit = this.handleSubmit.bind(this);
    
    }
    
    toggleModal() {
        this.setState({
          isModalOpen: !this.state.isModalOpen
        });
    }
    handleSubmit(values) {
        this.toggleModal();
        this.props.postComment(this.props.dishId, values.rating, values.author, values.comment);
    }
    
        render(){
      return(
          
         <div class="container">
          <Button outline onClick={this.toggleModal}><span className="fa fa-pencil fa-lg"></span> Submit</Button>
                    
          <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
          <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
          <ModalBody>
          <LocalForm onSubmit={(values)=>this.handleSubmit(values)}>
               <Row className="form-group" className="m-1">
                 <Label htmlfor="rating" >Rating</Label>
                  <Control.select model=".rating" name="rating"
                  className="form-control" >
                   <option>1</option>
                   <option>2</option>
                   <option>3</option>
                   <option>4</option>
                   <option>5</option>
                  </Control.select>
                </Row>
                <Row className="form-group" className="m-1">
                 <Label htmlfor="author" >Your Name</Label>
                  <Control.text  model=".author"  id="author" name="author"
                   placeholder="Enter your name" 
                   className="form-control"
                   validators={{
                      required,minlength:minLength(3),maxlength:maxLength(15)
                  }}/>
                  <Errors className="text-danger" model=".author" show="touched"
                  messages={{
                  required:'Required',
                  minlength:'Must be greater than 2 charachters',
                  maxlength:'Must be less than 15 charachters'
                  }} />
                </Row>
                <Row className="form-group" className="m-1">
                 <Label htmlfor="comment">Comment</Label>
                 <Control.textarea model=".comment" id="comment" name="comment" className="form-control" />
                </Row>
                <Button type="submit" value="submit" color="primary" className="m-1" >Submit </Button>
             </LocalForm>          
          </ModalBody>
      </Modal>
         </div>    
      
        );
  }  

}


function RenderDish({dish}) {
        if (dish != null)
            return(
             <FadeTransform
                in
                transformProps={{
                    exitTransform: 'scale(0.5) translateY(-50%)'
                }}>
                    <Card>
                        <CardImg top src={baseUrl + dish.image} alt={dish.name} />
                        <CardBody>
                            <CardTitle>{dish.name}</CardTitle>
                            <CardText>{dish.description}</CardText>
                        </CardBody>
                    </Card>
            </FadeTransform>
            );
        
    }

    function RenderComments({comments}){
        if (comments != null) {
            return(
                <div className="col-12 col-md-5 m-1">
                   <h4> Comments </h4>
                   <ul className="list-unstyled">
                   <Stagger in>
                        {comments.map((comment) => {
                            return (
                                <Fade in>
                                <li key={comment.id}>
                                <p>{comment.comment}</p>
                                <p>-- {comment.author} , {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(comment.date)))}</p>
                                </li>
                                </Fade>
                            );
                        })}
                    </Stagger>
                        
                   </ul>

                </div>
               
            );
        }
        else
            return(
                <div/>
            );  
    }
        
         
    
    const DishDetail = (props) => {
        console.log('DishDetail Component Render is invoked');
    
        if (props.isLoading) {
        return(
            <div className="container">
                <div className="row">            
                    <Loading />
                </div>
            </div>
        );
    }
    else if (props.errMess) {
        return(
            <div className="container">
                <div className="row">            
                    <h4>{props.errMess}</h4>
                </div>
            </div>
        );
    }
     else if(props.dish)
        {return(
            <div class="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                        <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>{props.dish.name}</h3>
                        <hr />
                    </div>                
                </div>
                <div className="row">
                <div className="col-12 col-md-5 m-1">
                    <RenderDish dish={props.dish}/>
                 </div>
                 <div className="col-12 col-md-5 m-1">
                 <RenderComments comments={props.comments}
                    />
                  <CommentForm dishId={props.dish.id} postComment={props.postComment} />
     
                 </div>
                </div>                                             
            </div>
            );
        }
        
        else
         {return(<div></div>);
         }   
       
     }


  
export default DishDetail; 


//{new Date(cmnts.date).toString().substr(0,10)} we can use this function also